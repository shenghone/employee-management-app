# employee-management-app
This is an employee management system build with React, GraphQL, MongoDB and PixiJs. You can find the deployed version here https://management-demo-app.surge.sh/

It is a full stack project. User can choose to register or use demo account to view the data. 

Front-end: React, Pixi, DayJs, Formik, ApolloClient, Yup
Back-end: Express, ApolloServer, Mongoose, Joi
